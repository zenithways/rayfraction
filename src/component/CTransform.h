#pragma once

#include "raylib_namespace.h"
#include "Component.h"

class CTransform : public Component
{
public:
    DEFINE_COMPONENT(CTransform);

    float rx, ry, scaleX, scaleY;
    rl::Vector2 position = { 0.f, 0.f };

    explicit CTransform(const EntityId& entity);

    explicit CTransform(const EntityId& entity, const rl::Vector2& position,
            const float& rotationX = 0.f, const float& rotationY = 0.f, const float& scaleX = 1.f,
            const float& scaleY = 1.f);

#ifdef DEBUG_TOOLS

    bool displayCross = false;

    void DebugGui() override;

    void DebugDraw() override;

#endif

};

class BinaryStream;

BinaryStream& operator<<(BinaryStream& stream, CTransform* obj);

BinaryStream& operator>>(BinaryStream& stream, CTransform* obj);
