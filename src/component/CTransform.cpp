#include "CTransform.h"

#include "Save.h"
#include "Debug.h"

CTransform::CTransform(const EntityId& entity)
        : Component(entity), rx(0.f), ry(0.f), scaleX(1.f), scaleY(1.f), position(rl::Vector2 { 0.f, 0.f })
{}

CTransform::CTransform(const EntityId& entity, const rl::Vector2& position, const float& rotationX,
        const float& rotationY, const float& scaleX, const float& scaleY)
        : Component(entity), rx(rotationX), ry(rotationY), scaleX(scaleX), scaleY(scaleY), position(position)
{}

#ifdef DEBUG_TOOLS

void CTransform::DebugGui()
{
    const bool previousDisplayCross = displayCross;
    ImGui::PushID(DEBUG_GUI_ID(CTransform));
    ImGui::SetNextItemOpen(true, ImGuiCond_Once);
    if(ImGui::CollapsingHeader("Transform"))
    {
        float x = position.x;
        ImGui::SetNextItemWidth(65.f);
        ImGui::DragFloat("##x", &x, 1.f, 0.f,
                         static_cast<float>(rl::GetScreenWidth()), "x: %.1f");

        float y = position.y;
        ImGui::SameLine();
        ImGui::SetNextItemWidth(65.f);
        ImGui::DragFloat("##y", &y, 1.f, 0.f,
                         static_cast<float>(rl::GetScreenHeight()), "y: %.1f");

        ImGui::SameLine();
        ImGui::Text("Position");

        ImGui::SameLine();
        ImGui::Checkbox(("Display##" + StringId()).c_str(), &displayCross);
        position = rl::Vector2 { x, y };
    }
    ImGui::PopID();

    if(displayCross != previousDisplayCross)
    {
        if(displayCross)
            Debug::AddToDrawList(this);
        else
            Debug::RemoveFromDrawList(this);
    }
}

void CTransform::DebugDraw()
{
    if(displayCross)
    {
        int x = static_cast<int>(position.x);
        int y = static_cast<int>(position.y);
        rl::DrawLine(x - 6, y, x + 5, y, rl::WHITE);
        rl::DrawLine(x, y - 6, x, y + 5, rl::WHITE);
    }
}

#endif

BinaryStream& operator<<(BinaryStream& stream, CTransform* t)
{
    stream << t->rx << t->ry << t->scaleX << t->scaleY << t->position.x << t->position.y;
    return stream;
}

BinaryStream& operator>>(BinaryStream& stream, CTransform* obj)
{
    stream >> obj->rx >> obj->ry >> obj->scaleX >> obj->scaleY >> obj->position.x >> obj->position.y;
    return stream;
}
