#pragma once

#if (DEBUG_LEVEL >= 2)
#include <string>
#include "Component.h"

class CDebugInfo : public Component
{
public:
    DEFINE_COMPONENT(CDebugInfo)

    int* width;
    int* height;

private:
    inline static std::map<std::string, int> nameIds_;
    std::string name_;
    int nameId_ = 0;

public:

    explicit CDebugInfo(const EntityId& entity) : Component(entity)
    {}

    std::string GetName() const
    {
        return name_ + "_" + std::to_string(nameId_);
    }

    void SetName(const std::string& nameToSet)
    {
        if(name_ == nameToSet)
            return;

        name_ = nameToSet;
        if(nameIds_.contains(name_))
        {
            nameId_ = ++nameIds_[name_];
        }
        else
        {
            nameIds_[name_] = 1;
            nameId_ = 1;
        }
    }

    void DebugGui() override {}
};
#endif
