#include "Game.h"

#include "raylib_namespace.h"
#include "implot.h"
#include "Scene.h"
#include "IconsFontAwesome6.h"

Game::Game()
        : scene_(nullptr)
{
    rl::InitWindow(1200, 750, "rayfraction - basic window");
    rl::InitAudioDevice();
}

Game::Game(int width, int height, const char* title)
        : scene_(nullptr)
{
    rl::InitWindow(width, height, title);
    rl::InitAudioDevice();
}

Game::~Game()
{
    delete scene_;
    scene_ = nullptr;
}

void Game::Exec()
{
    double previousTime = 0.;
    double dt = 0.;
    double accumulator = 0.;
    double fixedDt = 0.008;
    if(scene_ == nullptr)
    {
        scene_ = new Scene();
        scene_->Init();
    }
    controller_ = ControllerSystem();

#if (DEBUG_LEVEL == 0)
    rl::SetExitKey(rl::KEY_NULL);
#endif
#ifdef DEBUG_TOOLS
    rl::rlImGuiSetup(true);
    ImPlot::CreateContext();

    // Loading FontAwesome for ImGui icons
    ImGuiIO& imGuiIo = ImGui::GetIO();
    float baseFontSize = 13.0f; // 13.0f is the size of the default font. Change to the font size you use.
    // FontAwesome fonts need to have their sizes reduced by 2.0f/3.0f in order to align correctly
    // But we want big icons
    float iconFontSize = baseFontSize; // * 2.0f / 3.0f;
    // merge in icons from Font Awesome
    static const ImWchar iconsRanges[] = { ICON_MIN_FA, ICON_MAX_16_FA, 0 };
    ImFontConfig iconsConfig;
    iconsConfig.MergeMode = true;
    iconsConfig.PixelSnapH = true;
    iconsConfig.GlyphMinAdvanceX = iconFontSize;
    imGuiIo.Fonts->AddFontFromFileTTF("lib/iconFontCppHeaders/fa-solid-900.ttf", iconFontSize, &iconsConfig, iconsRanges);
    rl::rlImGuiReloadFonts();
#endif

    while(!rl::WindowShouldClose())    // Detect window close button or ESC key
    {
        dt = rl::GetTime() - previousTime;
        previousTime = rl::GetTime();
        accumulator += dt;
        controller_.Update();
        while(accumulator >= fixedDt)
        {
            Update(fixedDt);
            accumulator -= fixedDt;
        }
        rl::BeginDrawing();
        Draw();
        rl::EndDrawing();
    }

#ifdef DEBUG_TOOLS
    ImPlot::DestroyContext();
    rl::rlImGuiShutdown();
#endif
    rl::CloseWindow();
}

void Game::Update(double dt)
{
#ifdef DEBUG_TOOLS
    if(!Debug::GetPaused() || Debug::GetStep() > 0)
    {
        scene_->Update(dt);
        Debug::DecrementStep();
    }
#else
    scene_->Update(dt);
#endif
}

void Game::Draw()
{
    rl::ClearBackground(rl::BLACK);

    if(scene_ != nullptr)
    {
        scene_->Draw();

#if (DEBUG_LEVEL >= 2)
        rl::rlImGuiBegin();
        {
            scene_->DebugDraw();
        }
        rl::rlImGuiEnd();
#endif

    }
}

void Game::SwitchScene(IScene* newScene)
{
    delete scene_;
    scene_ = newScene;
    scene_->Init();
}
