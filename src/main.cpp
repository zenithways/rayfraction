#include <memory>

#define RAYMATH_IMPLEMENTATION
#include "raylib_namespace.h"
#undef RAYMATH_IMPLEMENTATION

#include "Game.h"
#include "Scene.h"

int main() {
    const int screenWidth = 1200;
    const int screenHeight = 750;
    std::unique_ptr<Game> game = std::make_unique<Game>();
    Scene* scene = new Scene();
    game->SwitchScene(scene);
    //rl::SetTargetFPS(60);
    game->Exec();
    return 0;
}
