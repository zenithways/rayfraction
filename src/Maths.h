#pragma once

#include <iostream>
#include <string>
#include "raylib_namespace.h"

namespace Maths
{
    /// @return The remains, once the value is clamped
    template<typename T>
    inline T Clamp(T& value, const T& min, const T& max)
    {
        if(value >= min && value <= max)
            return static_cast<T>(0);

        T result;
        if(value < min)
        {
            result = value - min;
            value = min;
        }
        if(value > max)
        {
            result = value - max;
            value = max;
        }
        return result;
    }

    template<>
    inline rl::Vector2 Clamp(rl::Vector2& value, const rl::Vector2& min, const rl::Vector2& max)
    {
        rl::Vector2 result;
        result.x = Clamp(value.x, min.x, max.x);
        result.y = Clamp(value.y, min.y, max.y);
        return result;
    }
};
