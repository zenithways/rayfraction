#include "Save.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <utility>

BinaryStream::BinaryStream()
    : buffer_(std::vector<Byte>())
{}

BinaryStream::BinaryStream(size_t bufferSize)
    : buffer_(std::vector<Byte>(bufferSize))
{}

void BinaryStream::Print()
{
    std::cout << std::hex << std::setfill('0');
    for(int i = 0; i < buffer_.size(); ++i)
    {
        std::cout << std::setw(2) << static_cast<int>(buffer_[i]) << ' ';
        if((i + 1) % 16 == 0)
            std::cout << '\n';
        else if((i + 1) % 4 == 0)
            std::cout << "\t";
        else if((i + 1) % 2 == 0)
            std::cout << ' ';
    }
    std::cout << std::endl;
}

SaveFile::SaveFile(std::string  filePath)
    : filePath_(std::move(filePath))
{}

bool SaveFile::Save()
{
    std::ofstream fileStream;
    fileStream.open(filePath_, std::ios::out | std::ios::trunc);
    if(!fileStream.is_open())
        return false;

    fileStream.write(buffer_.data(), buffer_.size());
    fileStream.close();
    return true;
}

bool SaveFile::Load()
{
    std::ifstream fileStream;
    fileStream.open(filePath_, std::ios::in | std::ios::binary);
    if(!fileStream.is_open())
        return false;

    buffer_ = std::vector<Byte>((std::istreambuf_iterator<Byte>(fileStream)), std::istreambuf_iterator<Byte>());
    fileStream.close();
    return !buffer_.empty();
}
