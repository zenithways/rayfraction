#pragma once

#include <cstring>
#include <vector>
#include <stdexcept>
#include "component/CTransform.h"

#define USE_CHAR_FOR_BYTE_TYPE
#ifdef USE_CHAR_FOR_BYTE_TYPE
using Byte = char;
#else
using Byte = std::byte;
#endif

class BinaryStream
{
protected:
    std::vector<Byte> buffer_;

private:
    size_t cursor_ = 0;

public:
    explicit BinaryStream();

    explicit BinaryStream(size_t bufferSize);

    template<typename T>
    BinaryStream& operator<<(const T& data)
    {
        static_assert(std::is_standard_layout_v<T>, "Stream insert operator << only accepts standard-layout types");
        if(cursor_ + sizeof(data) >= buffer_.capacity())
            buffer_.resize(buffer_.capacity() * 2 + sizeof(data));
        std::memcpy(&buffer_[cursor_], &data, sizeof(data));
        // Keeping the following code for archive. Does the same thing as memcpy
        //const Byte* byte = reinterpret_cast<const Byte*>(&data);
        //std::copy_n(byte, sizeof(data), &buffer_[cursor_]);
        cursor_ += sizeof(data);
        return *this;
    }

    template<typename T>
    BinaryStream& operator>>(T& data)
    {
        static_assert(std::is_standard_layout_v<T>, "Stream extract operator >> only accepts standard-layout types");
        if(cursor_ + sizeof(data) > buffer_.capacity())
            throw std::out_of_range("Trying to get data out of the range of the buffer");
        std::memcpy(&data, &buffer_[cursor_], sizeof(T));
        cursor_ += sizeof(T);
        return *this;
    }

    void Print();
};

class SaveFile : public BinaryStream
{
    std::string filePath_;

public:
    SaveFile() = delete;
    explicit SaveFile(std::string filePath);

    /// Truncate the save file and write the current buffer into it. If the file doesn't exists, it is created.
    /// @return False if failed to save, true otherwise
    bool Save();

    /// Load the save file to the buffer.
    /// @return False if failed to load, true otherwise
    bool Load();
};
