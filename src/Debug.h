#pragma once

#ifdef DEBUG_TOOLS

#include <climits>
#include <algorithm>
#include "raylib_namespace.h"
#include "Macros.h"
#include "Entity.h"
#include "Color.h"
#include "component/CDebugInfo.h"

#define DEBUG_GUI_ID(NAME) (#NAME + StringId()).c_str()

enum class Display : unsigned char
{
    NONE            = 0,
    MAIN_MENU       = 1 << 0,
    FPS_COUNTER     = 1 << 1,
    ENTITY_LIST     = 1 << 2,
    ENTITY_EXPLORER = 1 << 3,
    CONTROLS        = 1 << 4,
    IMGUI_DEMO      = 1 << 5,
    IMPLOT_DEMO     = 1 << 6,

    ALL             = UCHAR_MAX
}; FLAG(Display)

class Debug
{
private:
    inline static Display display_ = (Display::MAIN_MENU ^ Display::ENTITY_EXPLORER ^ Display::FPS_COUNTER
            ^ Display::ENTITY_LIST ^ Display::CONTROLS);
    inline static EntityId selectedEntity_ = ENTITY_ID_NULL;
    inline static int selectedEntityIndex_ = -1;
    inline static std::vector<Component*> drawList_;
    inline static bool colorPickerOpened_ = false;
    inline static rl::Color* updatedColor_;
    inline static bool paused_ = false;
    inline static unsigned int step_ = 0;

public:
    static void Draw();
    static void SwitchFpsDisplay();
    static void SwitchMainMenu();
    static void SwitchEntityExplorer();
    static void SwitchEntityList();
    static void SwitchControls();
    static void SwitchImGuiDemo();
    static void SwitchImPlotDemo();
    static void SelectEntity(const EntityId& entity);
    static void DisplayColorPicker(rl::Color* colorToUpdate);
    static void AddToDrawList(Component* component);
    static void RemoveFromDrawList(Component* component);
    static bool GetPaused();
    static unsigned int GetStep();
    static void DecrementStep();

private:
    static void ShowMainMenu();
    static void ShowFpsCounter();
    static void ShowEntityExplorer();
    static void ShowEntityList();
    static void ShowControls();
    static void DrawColorPicker();
};

#endif
