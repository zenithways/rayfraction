#pragma once

#include "raylib_namespace.h"

template<typename T>
struct Vector2
{
    T x;
    T y;

    T operator+(const Vector2<T>& other) const
    {
        return Vector2<T> { x + other.x, y + other.y };
    }

    T operator-(const Vector2<T>& other) const
    {
        return Vector2<T> { x - other.x, y - other.y };
    }

    T& operator+=(const Vector2<T>& other)
    {
        x += other.x;
        y += other.y;
        return *this;
    }

    T& operator-=(const Vector2<T>& other)
    {
        x -= other.x;
        y -= other.y;
        return *this;
    }

    /*auto operator<=>(const Vector2<T>& other) const
    {
        return x <=> other.x && y <=> other.y;
    }*/

    bool operator==(const Vector2<T>& other) const
    {
        return x == other.x && y == other.y;
    }

    bool operator!=(const Vector2<T>& other) const
    {
        return !(*this == other);
    }

    rl::Vector2 ToRlVector() const
    {
        return rl::Vector2 { static_cast<float>(x), static_cast<float>(y) };
    }

    inline static Vector2<T> FromRlVector(rl::Vector2 vector)
    {
        return Vector2<T> { static_cast<T>(vector.x), static_cast<T>(vector.y) };
    }
};
