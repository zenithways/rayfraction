/**
 * Inspired by Jeffery Myers https://github.com/JeffM2501/raylib_ecs_sample/blob/main/test/components.h
 */

#pragma once

#include <memory>
#include <string>
#include "imgui.h"
#include "Alias.h"
#include "ComponentStore.h"

class ComponentIdHandler
{
    static ComponentId id_;

public:
    template<typename ComponentT>
    static const ComponentId GetId()
    {
        static const ComponentId result = ++id_;
        return result;
    }
};

inline constexpr ComponentId COMPONENT_ID_NULL = 0;

#define DEFINE_COMPONENT(TYPE) \
    static ComponentId GetBaseTypeId() { return ComponentIdHandler::GetId<TYPE>(); } \
    static ComponentId GetTypeId() { return ComponentIdHandler::GetId<TYPE>(); }

#define DEFINE_COMPONENT_DERIVED(BASE, TYPE) \
    static ComponentId GetBaseTypeId() { return  ComponentIdHandler::GetId<BASE>(); } \
    static ComponentId GetTypeId() { return  ComponentIdHandler::GetId<TYPE>(); }

class Component
{
    ComponentId id_;
    EntityId entityId_;
    bool pendingDelete_;

public:
    explicit Component(const EntityId& entity)
        : id_(ComponentStore::NextId()), entityId_(entity)
    {}
    virtual ~Component() = default;

    static ComponentId GetTypeId();
    static ComponentId GetBaseTypeId();

    virtual void PendingDelete();

    ComponentId GetId() const;

    EntityId GetEntityId() const;

#ifdef DEBUG_TOOLS

    std::string StringId() const;
    virtual void DebugGui();
    virtual void DebugDraw();

#endif
};
