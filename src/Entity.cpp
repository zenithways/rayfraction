#include "Entity.h"

#include "Alias.h"
#if (DEBUG_LEVEL >= 2)
#include "component/CDebugInfo.h"
#endif

namespace EntityBuilder
{
    namespace
    {
        EntityId currentEntityId = ENTITY_ID_NULL;
    }

    EntityId Create(const std::string& name)
    {
        EntityId result = ++currentEntityId;
        name; // Removing warning C4100 on MSVC
#ifdef DEBUG_TOOLS
        auto debugInfo = ComponentStore::Build<CDebugInfo>(result);
        debugInfo->SetName(name);
#endif
        return result;
    }

    EntityId Create()
    {
        EntityId result = ++currentEntityId;
#ifdef DEBUG_TOOLS
        ComponentStore::Build<CDebugInfo>(result);
#endif
        return result;
    }

    void Delete(EntityId entity)
    {
        ComponentStore::DeleteAll(entity);
    }
}
