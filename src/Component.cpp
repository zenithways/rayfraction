#include "Component.h"

ComponentId ComponentIdHandler::id_ = 0;

ComponentId Component::GetTypeId()
{
    return COMPONENT_ID_NULL;
}

ComponentId Component::GetBaseTypeId()
{
    return COMPONENT_ID_NULL;
}

void Component::PendingDelete()
{
    pendingDelete_ = true;
    ComponentStore::EndDelete(GetEntityId(), this);
}

EntityId Component::GetEntityId() const
{
    return entityId_;
}

ComponentId Component::GetId() const
{
    return id_;
}

#ifdef DEBUG_TOOLS

std::string Component::StringId() const
{
    return std::to_string(id_);
}

void Component::DebugGui()
{
    ImGui::BulletText("Component");
    ImGui::SameLine();
    ImGui::TextDisabled("?");
    if (ImGui::IsItemHovered())
    {
        ImGui::BeginTooltip();
        ImGui::PushTextWrapPos(ImGui::GetFontSize() * 35.0f);
        ImGui::TextUnformatted("This component type hasn't any defined debug ui");
        ImGui::PopTextWrapPos();
        ImGui::EndTooltip();
    }
}

void Component::DebugDraw()
{}

#endif
