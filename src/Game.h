#pragma once

#include <memory>
#include "Controller.h"

class Game
{
private:
    ControllerSystem controller_;
    class IScene* scene_;
    bool isSetup = false;

public:
    Game();

    Game(int width, int height, const char* title);

    ~Game();

    void Exec();

    void Update(double dt);

    void Draw();

    void SwitchScene(IScene* newScene);
};
