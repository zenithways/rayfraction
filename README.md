# Rayfraction

Rayfraction is an ECS game framework built on top
of [raylib](https://github.com/raysan5/raylib) and
using [rlImGui](https://github.com/raysan5/raylib) as a debug tool

## Note

This project is mainly for learning purposes (and making games in an environment
I understand). Some choices may be debatable because of the existence of a
higher level counterpart, but they help me improve and learn.

The clang-tidy is build following the same logic: I have set it quite
restrictive and remove rules I understand but won't follow, piece by piece. This
means the file is not a set of rules to follow absolutely (especially
`readability-*` checks).

This whole project is build to experiment and isn't suited to be used by anyone
but me, but feel free to test it if you want.
